# NBOS-Php-Module-API-Server-codeigniter
PHP CodeIgniter Starter API server

PreRequisite:

 MAMP for MAc
 LAMP for Linux
 XAMPP/WAMP for Windows

This is the module API server using NBOS SDK

1) Clone code
git clone git@gitlab.com:wavelabs/idn-api-php.git
 
2) Create DB
Create Database: nbos_module_todo
Import DB SQL from /application/data/nbos_module_todo.sql

3) Add Virutalhost pointing to codebase
<VirtualHost *:80>
    DocumentRoot "/fullpathof/idn-api-php/"
    ServerName php-todo-api.server
</VirtualHost>

4)Add a host entry in /etc/hosts:
127.0.0.1 php-todo-api.server

5) Restart APACHE server







