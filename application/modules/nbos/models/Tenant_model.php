<?php
/**
 * Created by PhpStorm.
 * User: nbmac4
 * Date: 7/31/16
 * Time: 10:30 AM
 */

class Tenant_model extends CI_Model {

    public $newTenant=false;
    public function __construct()
    {
        $this->load->database();
    }

    public function isActive($tenantId)
    {

        $query = $this->db->get_where('nbos_tenant', array( 'active'=>1, 'tenant_id' => $tenantId));
        if(count($query->row_array()) > 0) {
            return true;
        }
        return false;
    }
    public function isModuleEnabled($moduleToken, $moduleName){
        // call nbos sdk to figure out if tenant is subscribed to module

        if(!$this->isActive($moduleToken->getTenantId())){
            if(is_array($moduleToken->getModules()) && count($moduleToken->getModules()) > 0){
                $this->newTenant = true;
                $data = array(
                    'tenant_id' => $moduleToken->getTenantId(),
                    'tenant_name' => substr(strstr($moduleToken->getTenantId(), ":"),1),
                    'active' => 1
                );
                $this->db->insert('nbos_tenant', $data);
                return $this->db->affected_rows() > 0;
            }else{
               return false;
            }
        }
        return true;
    }
    public function isNewTenant(){
        return $this->newTenant;
    }

}