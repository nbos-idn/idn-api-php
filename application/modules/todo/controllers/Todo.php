<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Todo extends API_Controller {


    function __construct()
    {

        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('todo_model');

    }

    public function index()
    {
        $this->response_error([
            "messageCode" => "todo.endpoint",
            "message" => "Invalid endpoint"
        ]);
    }

    public function list_get(){

        if($this->moduleToken->isMember()){
            // User todo
            $data = $this->todo_model->get_user_todos($this->moduleToken->getTenantId(), $this->moduleToken->getUserId());
            $this->response_success($data);
        }else{
            // Public TODO
            $data = $this->todo_model->get_tenant_todos($this->moduleToken->getTenantId());
            $this->response_success($data);
        }
    }
    public function mylist_get(){
        /*
         * GET all todo's of Token Member
         */
        if($this->moduleToken->isMember()){

            $data = $this->todo_model->get_user_todos($this->moduleToken->getTenantId(), $this->moduleToken->getUserId());
            $this->response_success($data);
        }else{
            $this->response_error([
                "messageCode" => "todo.user",
                "message" => "Invalid user token"
            ]);
        }
    }

    public function create_post(){

        if($this->moduleToken->isMember()){
            if($this->input->post("title") == ''){
                $this->setError("todo.title", "Title empty",'title');
            }
            if($this->input->post("description") == ''){
                $this->setError("todo.description", "Title empty",'desc');
            }
            if($this->hasErrors()) {
                $this->response_error();
            }else {
                $this->todo_model->save($this->moduleToken->getTenantId(), $this->moduleToken->getUserId());
                $this->response_success(array('status'=>true,"message"=>"Todo added successfully"));
            }
        }else{
            $this->response_forbidden([
                "messageCode" => "module.user",
                "message" => "Invalid user token"
            ]);
        }
    }
    public function modify_put($id){
        if($this->moduleToken->isMember()){
            $todoId = intval($id);
            if(!$todoId){
                $this->setError("todo.id", "TODO unique id empty",'id');
            }
            $data = $this->put();
            if($data["title"] == ''){
                $this->setError("todo.title", "Title empty",'title');
            }
            if($data["description"] == ''){
                $this->setError("todo.description", "Title empty",'desc');
            }
            if($this->hasErrors()) {
                $this->response_error();
            }else {

                if($this->todo_model->save($this->moduleToken->getTenantId(), $this->moduleToken->getUserId(), $todoId, $data)) {
                    $this->response_success(array('status'=>true,"message"=>"Todo Updated successfully"));
                }else{
                    $this->response_success(array('status'=>true,"message"=>"No update in title and desc"));
                }
            }
        }else{
            $this->response_forbidden([
                "messageCode" => "module.user.unauthorized",
                "message" => "Unauthorized to update others TODO"
            ]);
        }

    }
    public function remove_delete($id){
        if($this->moduleToken->isMember() && $this->moduleToken->hasAuthority('authority:TodoPhp.manager.create')){
            if($this->todo_model->delete($id)){
                $this->response_success(array('status'=>true,"message"=>"Todo deleted successfully"));
            }else{
                $this->response_error([
                    "messageCode" => "todo.delete",
                    "message" => "Unable to delete todo"
                ]);
            }
        }else{
            $this->response_forbidden([
                "messageCode" => "module.user.unauthorized",
                "message" => "Unauthorized to delete TODO"
            ]);
        }
    }


}