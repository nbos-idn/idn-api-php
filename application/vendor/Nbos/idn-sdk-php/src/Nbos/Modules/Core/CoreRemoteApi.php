<?php
/**
 * Created by PhpStorm.
 * User: nbmac4
 * Date: 7/21/16
 * Time: 10:48 AM
 */

namespace Nbos\Modules\Core;


use Nbos\Api\SuccessResponse;
use Nbos\Api\TokenApiModel;

class CoreRemoteApi {
    public $baseCoreUrl = "/api/core/v0";

    public function __construct($httpClient) {
        $this->httpClient = $httpClient;
    }

    private function getTenantModuleUrl($tenantId){
        return $this->baseCoreUrl . "/tenants/".$tenantId."/modules";
    }
    public function getTenantModules($tenantId, $moduleName){
        return $this->httpClient->get($this->getTenantModuleUrl($tenantId), '', true, '', $moduleName);
    }

    private function getTenantModuleConfigUrl($tenantId, $moduleId){
        return $this->baseCoreUrl . "/config/values/tenant/".$tenantId."/".$moduleId;
    }
    public function getTenantModuleConfig($tenantId, $moduleId, $moduleName, $moduleKey){
        return $this->httpClient->get($this->getTenantModuleConfigUrl($tenantId, $moduleId), '', true, '', $moduleName, $moduleKey);
    }
} 